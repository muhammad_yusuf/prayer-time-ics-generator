import argparse
import csv
import ics
import arrow
from datetime import timedelta
from dateutil import tz

#
# Use the following code to convert prayer times in csv file into calendar.ics format
# so that you can import it and block your calendar and be notified.
# The code add 10 minutes notification and block the calendar for 30 minutes
# from the start of the prayer.

# You can specify the csv file name and the time zone.
# The Format of the csv file is compatible with the files downloaded from https://www.salahtimes.com/egypt/cairo
# A header contains date,Fajr,SunRise,Duhr,Asar,Maghrib,Ishah only.

# The code generated with high support from ChatGpt.

# Any Muslim are allowed to change or use this script for the same purpose, the code is opensource.

# usage :
# python script.py -f my_events.csv -t America/New_York

# default file name is events.csv, default timezone is EET
#

# Parse the command line arguments
parser = argparse.ArgumentParser(description='Convert a CSV file of events to an ICS calendar')
parser.add_argument('-f', '--file', default='events.csv', help='name of the CSV file')
parser.add_argument('-t', '--timezone', default='EET', help='time zone of the events')
args = parser.parse_args()

# Open the CSV file and read its contents into a list
with open(args.file, 'r') as f:
    reader = csv.reader(f)
    all_days_prayers = list(reader)

# Retrieve the event names from the first row (the header)
event_names = all_days_prayers[0][1:]

# Skip the first row (the header)
events = all_days_prayers[1:]

# Create an ICS calendar
cal = ics.Calendar()

# Iterate over the events and add them to the calendar
for i, one_day_prayers in enumerate(events):
    day_date = one_day_prayers[0]
    prayers_times = one_day_prayers[1:]
    for j, prayer_time in enumerate(prayers_times):
        # Concatenate the date and time columns
        start = f"{day_date} {prayer_time}"
        # Convert the start time to the ISO 8601 format
        start_dt = arrow.get(start, 'ddd D MMM YYYY HH:mm')
        # Convert the start time to the specified timezone
        start_dt_tz = start_dt.replace(tzinfo=args.timezone)
        # Add 25 minutes to the start time to get the end time
        end_dt_tz = start_dt_tz.shift(minutes=+30)
        # Convert the start and end times to the ISO 8601 format
        start = start_dt_tz.isoformat()
        end = end_dt_tz.isoformat()
        # Create an ICS event
        e = ics.Event()
        # Use the event name from the header as the name of the event
        e.name = event_names[j]
        # Add description to tell you leave every thing to pray.
        e.description = "اتق الله وقم لصلاتك"
        e.begin = start
        e.end = end
        # Set the timezone of the event to the specified timezone
        e.timezone = args.timezone
        # Add an alarm that goes off 10 minutes before the start of the event
        e.alarms = [ics.DisplayAlarm(trigger=timedelta(minutes=-10))]

        # Add the event to the calendar
        cal.events.add(e)

# Write the calendar to a file
with open('prayer_calendar.ics', 'w') as f:
    f.writelines(cal)
